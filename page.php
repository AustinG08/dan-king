<?php get_header(); ?>
  <div id="content" role="main" class="page">
    <?php
    if (have_posts()):
      while(have_posts()):
        the_post();
        the_content();
      endwhile;
    endif;
    ?>
  </div><!--//content-->
<?php
//get_sidebar();
get_footer();
?>