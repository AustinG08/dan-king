<?php get_header(); ?>
<div id="content" role="main" class="blog single">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <article <?php post_class() ?> id="post-<?php the_ID(); ?>" itemscope='' itemtype='http://schema.org/BlogPosting'>
      <header>
      <?php echo '<meta content="' . get_the_time() . '" />'; ?>
      <h1 itemprop="name headline" class="entry-title"><?php the_title(); ?></h1>
      <div class="post-meta">
        <div class="categories">
          Posted on <span class="date updated"><?php echo '<meta itemprop="datePublished" content="' . get_the_date("Y-m-d") . '" />'; echo get_the_date("M d, Y"); ?></span> in <?php echo get_the_category_list(', ') ?> by <a href="<?php echo get_author_posts_url($post->post_author)?>"><span itemprop="author" class="vcard author"><span class="fn"><?php echo get_the_author()?></span></span></a>
          </div><!--//categories-->
        </div><!--//post-meta-->
      </header>
      <div class="clear"></div>
      <section class="content" itemprop='articleBody'>
       <?php the_content(); ?>
      </section><!--//content-->
    </article>
    <?php endwhile; else: ?>
    <p>Sorry, no posts matched your criteria.</p>
    <?php endif; ?>
    <?php echo do_shortcode('[share_buttons]'); ?>
</div><!--//content-->
<?php get_footer(); ?>