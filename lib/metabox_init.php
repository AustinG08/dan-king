<?php
class the_bare_necessities_metabox_init{

  function __construct( ) {
    add_action( 'init', array($this,'metabox_require'), 9999 );
	add_action( 'cmb_render_number', array($this,'number_field'), 10, 2 );
    add_filter( 'cmb_validate_number', array($this,'number_field_validate') );
	add_filter( 'cmb_show_on', array($this,'show_on_inside_pages'), 10, 2 );
	add_filter( 'cmb_validate_page_select', array($this, 'page_select_field_validate') );
	add_action( 'cmb_render_page_select', array($this, 'page_select_field'), 10, 2 );
	add_action( 'cmb_render_email', array($this, 'email_field'), 10, 2 );
	add_filter( 'cmb_validate_email', array($this, 'email_field_validate') );
  }
  
  //require framework
  function metabox_require() {
    //if the metabox class doesn't exist, go get it
    if ( ! class_exists( 'cmb_Meta_Box' ) ){
      require_once get_template_directory() . '/vendors/metaboxes/init.php';
    }
  }
  
  //add number field
  function number_field( $field, $meta ) {
    echo '<input type="number" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" style="width:10%" />','<p class="cmb_metabox_description">', $field['desc'], '</p>';
  }
  
  //add number field validation
  function number_field_validate( $new ) {
    return preg_replace('/[^0-9]/', '', $new);
  }
  
  //add show_on filtet for inside pages
  function show_on_inside_pages( $display, $meta_box ) {
    if ( 'inside_pages' !== $meta_box['show_on'] ){
      return $display;
    }
   
    // Get the current ID
    if ( isset( $_GET['post'] ) ) {
      $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
      $post_id = $_POST['post_ID'];
    }
    
    //Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option('page_on_front');
   
    //return false early if there is no ID
    if( !isset( $post_id ) OR $post_id == $front_page ){
      return false;
    }
   
    return $display;
  }
  
  
  //add page select field
  function page_select_field( $field, $meta ) {
	$args = array(
	  'depth'            => 0,
	  'child_of'         => 0,
	  'show_option_none' => 'Select a Page',
	  'selected'         => $meta,
	  'echo'             => 1,
	  'name'             => $field['id']
	);
	wp_dropdown_pages($args);
	echo '<p class="cmb_metabox_description">', $field['desc'], '</p>';
  }//end page_select_field
  
  //add page select validation
  function page_select_field_validate( $new ) {
	if ( !is_numeric( $new ) ) {$new = "";}   
	return $new;
  }//end page_select_field_validate
  
  //add email field
  function email_field( $field, $meta ) {
	echo '<input type="email" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" style="width:97%" />','<p class="cmb_metabox_description">', $field['desc'], '</p>';
  }
  
  //validate email field
  function email_field_validate( $new ) {
	if ( !is_email( $new ) ) {
	  $new = "";
	}   
	return $new;
  }
  
  // metaboxes from
  // https://github.com/WebDevStudios/Custom-Metaboxes-and-Fields-for-WordPress
  // see vendors/metaboex/example-functions.php
}//end the_bare_necessities_metabox_init

$the_bare_necessities_metabox_init = new the_bare_necessities_metabox_init();