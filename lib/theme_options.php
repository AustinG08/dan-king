<?php
if ( !class_exists( 'ReduxFramework' ) AND file_exists( get_template_directory() . '/vendors/redux-framework/ReduxCore/framework.php' ) ) {
  require_once( get_template_directory() . '/vendors/redux-framework/ReduxCore/framework.php' );
}

class the_bare_necessities_theme_options{
    
    public $args = array();
    public $sections = array();
    public $theme;
    public $ReduxFramework;

    public function __construct( ) {

      // Save theme info
      $this->theme = wp_get_theme();

      // Set the default arguments
      $this->setArguments();

      // Create the sections and fields
      $this->setSections();
      
      if ( !isset( $this->args['opt_name'] ) ) { // No errors please
        return;
      }
      
      $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);

    }
    
    /**===============================================================================================================================
      ACTUAL DECLARATION OF SECTIONS
      Each section is multidimensional array
      Visit the default settings page for more info on configuration
===============================================================================================================================**/

    public function setSections() {
      
      //Information about how to implement settings. THIS SHOULD BE REMOVED AFTER YOU KNOW WHAT YOU ARE DOING
      $this->sections[] = array(
        'icon' => 'el-icon-info-sign',
        'title' => __('Dev Information', 'the_bare_necessities'),
        'desc' => __('
	    <h4>This section should be removed commented out before going to the test server!</h4>
	    <h3>Why should I include theme options?</h3>
	    <h4>Because hard-coding things in the theme now means you will likely get a maintenance request to change it in the future.</h4>
	    <p>We want to require as little developer intervention after the initial build as possible</p>
	    <p>Some examples of items to use options for:</p>
	    <ul style="margin-left: 2em; list-style: disc;">
		<li>Social media links</li>
		<li>Phone number</li>
		<li>Template links</li>
		<li>Analytics/Footer JS</li>
		<li>Banner Images</li>
	    </ul>
	    <h4>Holy crap, those would all be useful!</h4>
	    <p>Glad you think so, because they are already setup.</p>
	    <p>You should definitely add some more though, so read on.</p>
	    <h3>Fields:</h3>
	    <p>There are a lot of fields available. Check out these resources:</p>
	    <ul style="margin-left: 2em; list-style: disc;">
		<li><a href="http://reduxframework.com/docs/user-guide/fields/" target="_blank">Official Documentation</a></li>
		<li><a href="https://github.com/ReduxFramework/ReduxFramework/wiki/Fields" target="_blank">Documentation on GitHub</a></li>
		<li><a href="https://github.com/ReduxFramework/ReduxFramework/blob/master/sample/sample-config.php" target="_blank">The sample config file</a></li>
	    </ul>
	    <h3>Icons:</h3>
	    <p>Icons are used in many places, such as the section buttons on the left</p>
	    <p>Icon slugs can be found <a href="http://shoestrap.org/downloads/elusive-icons-webfont/" target="_blank">here</a></p>
	    <h3>Where do I edit this code?</h3>
	    <p><code>' . dirname(__FILE__) . '/theme_options.php</code></p>
	    <p>This is where theme options are stored. The Redux Framework is already initiated, you just need to update the fields to suite your needs.</p>
	    <p>Do a search for "ACTUAL DECLARATION OF SECTIONS" to get to the right place in the file.</p>
	    <h3>Retrieving Options</h3>
	    <p>What good are options if you can\'t easily use them?</p>
	    <ol style="margin-left: 2em; list-style: decimal;">
		<li>Grab the global variable for theme options<p><code>&lt;?php global $theme_options; ?&gt;</code></p></li>
		<li>Get the field you need with the id set in theme_options.php<p><code>&lt;?php $facebook_url = $theme_options[\'facebook_url\']; ?&gt;</code></p></li>
		<li>Do whatever you want with the option. I would suggest something like checking if it is empty before use</li>
	    </ol>
	    <p>Check out the test page (and it\'s page template) for an example.</p>
	    <p>Hint: Page selects store post_id. Use <a href="http://codex.wordpress.org/Function_Reference/get_permalink" target="_blank"><code>get_permalink()</code></a> for the URL</p>
	', 'the_bare_necessities'),
        'fields' => array()

      );
      
      //Contact Information
      $this->sections[] = array(
        'icon' => 'el-icon-pencil',
        'title' => __('Contact Information', 'the_bare_necessities'),
        'desc' => __('', 'the_bare_necessities'),
        'fields' => array(
	    //To create more just duplicate this array and update id/title
	    array(
		'id'       => 'phone_number',
		'type'     => 'text',
		'title'    => __('Phone Number', 'the_bare_necessities'),
		'subtitle'    => __('In header and footer', 'the_bare_necessities'),
		'desc'     => __('10 digit phone number', 'the_bare_necessities'),
		'validate' => 'no_html',
		'default'  => '',
		'placeholder' => '123-456-7890'
	    ),
	)

      );
      
      
      //Homepage
      $this->sections[] = array(
        'icon' => 'el-icon-home',
        'title' => __('Home Page', 'the_bare_necessities'),
        'desc' => __('', 'the_bare_necessities'),
        'fields' => array(
	    //Got more than one banner image? Check out the slides field
	    array(
		'id'       => 'banner_image',
		'type'     => 'media', 
		'width' => '1534',
		'height' => '484',
		'title'    => __('Banner Image', 'redux-framework-demo'),
		'desc'     => __('Make sure the image is 1534px by 484px', 'redux-framework-demo'),
	    ),
	)

      );
      
      
      //Social Media
      $this->sections[] = array(
        'icon' => 'el-icon-group',
        'title' => __('Social Media', 'the_bare_necessities'),
        'desc' => __('Don\'t have one of these? That\'s ok, just leave it blank', 'the_bare_necessities'),
        'fields' => array(
	    array(
		'id'       => 'facebook_url',
		'type'     => 'text',
		'title'    => __('Facebook URL', 'the_bare_necessities'),
		'desc'     => __('Full URL - including things like http:// and www.', 'the_bare_necessities'),
		'validate' => 'url',
		'default'  => '',
		'placeholder' => 'https://www.facebook.com/'
	    ),
	    array(
		'id'       => 'twitter_url',
		'type'     => 'text',
		'title'    => __('Twitter URL', 'the_bare_necessities'),
		'desc'     => __('Full URL - including things like http:// and www.', 'the_bare_necessities'),
		'validate' => 'url',
		'default'  => '',
		'placeholder' => 'https://www.twitter.com/#!/'
	    ),
	    array(
		'id'       => 'gplus_url',
		'type'     => 'text',
		'title'    => __('Google+ URL', 'the_bare_necessities'),
		'desc'     => __('Full URL - including things like http:// and www.', 'the_bare_necessities'),
		'validate' => 'url',
		'default'  => '',
		'placeholder' => 'https://plus.google.com/'
	    ),
	)

      );
      
      //SEO
      $this->sections[] = array(
        'icon' => 'el-icon-search',
        'title' => __('SEO', 'the_bare_necessities'),
        'desc' => __('Note: These are global - they print on all pages', 'the_bare_necessities'),
        'fields' => array(
	    array(
		'id'       => 'footer_code',
		'type'     => 'ace_editor',
		'title'    => __('Footer Code', 'the_bare_necessities'), 
		'subtitle' => __('Printed just before the &lt;/body&gt; tag<p>Put things like Analytics and call tracking here.</p>', 'the_bare_necessities'),
		'mode'     => 'js',
		'theme'    => 'monokai',
		'default'  => '<!-- code from theme options -->'
	    ),
	    array(
		'id'       => 'head_code',
		'type'     => 'ace_editor',
		'title'    => __('&lt;head&gt; Code', 'the_bare_necessities'), 
		'subtitle' => __('Put things like Tag Manager code here.', 'the_bare_necessities'),
		'mode'     => 'js',
		'theme'    => 'monokai',
		'default'  => '<!-- code from theme options -->'
	    ),
	)

      );
      
      //Template Links
      $this->sections[] = array(
        'icon' => 'el-icon-link',
        'title' => __('Template Links', 'the_bare_necessities'),
        'desc' => __('Have a link you can\'t edit in the content? Chances are you can update it here', 'the_bare_necessities'),
        'fields' => array(
	    //To create more just duplicate this array and update id/title
	    array(
		'id'       => 'contact_page',
		'type'     => 'select',
		'title'    => __('Contact Page', 'the_bare_necessities'), 
		'subtitle' => __('Linked in header/sidebar', 'the_bare_necessities'),
		'desc'     => __('Search by page name', 'the_bare_necessities'),
		'data' => 'page'
	    ),
	)

      );
      
    }
    
    /**
      
      All the possible arguments for Redux.
      For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments

     **/
    public function setArguments() {
      
      $theme = wp_get_theme(); // For use with some settings. Not necessary.

      $this->args = array(
              
        // TYPICAL -> Change these values as you need/desire
        'opt_name' => 'the_bare_necessities_theme_options', // This is where your data is stored in the database and also becomes your global variable name.
        'display_name' => $theme->get('Name') . ' Options', // Name that appears at the top of your panel
        'display_version' => $theme->get('Version'), // Version that appears at the top of your panel
        'menu_type' => 'menu', //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu' => true, // Show the sections below the admin menu item or not
        'menu_title' => __( 'Theme Options', 'the_bare_necessities' ),
              'page' => __( 'Theme Options', 'the_bare_necessities' ),
              'google_api_key' => 'AIzaSyC5vV5-6iwhDBlCEUiKSs41oIvlT3PUAJ0', // Must be defined to add google fonts to the typography module
              'global_variable' => 'theme_options', // Set a different name for your global variable other than the opt_name
              'dev_mode' => false, // Show the time the page took to load, etc
              'customizer' => true, // Enable basic customizer support

              // OPTIONAL -> Give you extra features
              'page_permissions' => 'edit_others_posts', // Permissions needed to access the options panel.
        );
      
   
      // Panel Intro text -> before the form. HTML accepted
      $this->args['intro_text'] = __('
	<h3>Careful, once you save or reset there\'s no going back<sup>*</sup></h3>
	<h4>*Unless you exported the current settings somewhere safe first</h4>', 'the_bare_necessities
      ');

      // Add content after the form. HTML accepted
      $this->args['footer_text'] = __('');

    }
    
}//end the_bare_necessities_theme_options

$the_bare_necessities_theme_options = new the_bare_necessities_theme_options();