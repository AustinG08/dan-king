<?php
class the_bare_necessities_add_post_id_to_admin{
    
    //add actions/hooks in construct
    function __construct(){
        add_filter('manage_posts_columns', array($this, 'admin_post_id_column_label'), 5);
        add_action('manage_posts_custom_column', array($this, 'admin_post_id_column_value'), 5, 2);
        add_filter('manage_pages_columns', array($this, 'admin_post_id_column_label'), 5);
        add_action('manage_pages_custom_column', array($this, 'admin_post_id_column_value'), 5, 2);
    }//end __construct
    
    function admin_post_id_column_label($defaults){
        $defaults['wps_post_id'] = __('ID');
        return $defaults;
    }// end admin_post_id_column_label function
  
    function admin_post_id_column_value($column_name, $id){
        if($column_name === 'wps_post_id'){
            echo $id;
        }
    }// end admin_post_id_column_value function
    
}//end the_bare_necessities_add_post_id_to_admin

$the_bare_necessities_add_post_id_to_admin = new the_bare_necessities_add_post_id_to_admin();