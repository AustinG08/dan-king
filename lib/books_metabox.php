<?php
class the_bare_necessities_books_metabox{
    
    //add actions/hooks in construct
    function __construct(){
        add_filter( 'cmb_meta_boxes', array($this,'define_metabox') );
    }//end __construct
    
    
    /**
     * Define the metabox and field configurations.
     *
     * @param  array $meta_boxes
     * @return array
     */
    function define_metabox( array $meta_boxes ) {
    
      // Start with an underscore to hide fields from custom fields list
      $prefix = '_book_';
    
      $meta_boxes['book_metabox'] = array(
        'id'         => 'book_metabox',
        'title'      => __( 'Book Details', 'books' ),
        'pages'      => array( 'book', ), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
        // 'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
        'fields'     => array(
          array(
            'name' => __( 'Authors', 'books' ),
            'desc' => __( 'Who wrote this book?', 'books' ),
            'id'   => $prefix . 'authors',
            'type' => 'text',
             'repeatable' => true,
            // 'on_front' => false, // Optionally designate a field to wp-admin only
          ),
          array(
            'name' => __( 'Contact Email', 'books' ),
            'desc' => __( 'Typically the Publisher', 'books' ),
            'id'   => $prefix . 'email',
            'type' => 'email',
          ),
          array(
            'name' => __( 'ISBN Number', 'books' ),
            'desc' => __( '10 or 13 digit', 'books' ),
            'id'   => $prefix . 'isbn',
            'type' => 'text_small',
            // 'repeatable' => true,
          ),
          array(
            'name' => __( 'Website URL', 'books' ),
            'desc' => __( 'Full URL, including http://, www., etc.', 'books' ),
            'id'   => $prefix . 'url',
            'type' => 'text_url',
            // 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
            // 'repeatable' => true,
          ),
          array(
            'name' => __( 'Publication Date', 'books' ),
            'id'   => $prefix . 'date',
            'type' => 'text_date',
          ),
          array(
            'name'   => __( 'Price', 'books' ),
            'desc'   => __( 'US Dollars', 'books' ),
            'id'     => $prefix . 'price',
            'type'   => 'text_money',
            // 'before' => '�', // override '$' symbol if needed
            // 'repeatable' => true,
          ),
          array(
            'name'    => __( 'Type', 'books' ),
            'id'      => $prefix . 'type',
            'type'    => 'select',
            'options' => array(
              array( 'name' => __( 'Select a Type', 'books' ), 'value' => '', ),
              array( 'name' => __( 'Fiction', 'books' ), 'value' => 'fiction', ),
              array( 'name' => __( 'Non-Fiction', 'books' ), 'value' => 'non_fiction', )
            ),
          ),
          array(
            'name'    => __( 'Conditon', 'books' ),
            'id'      => $prefix . 'condition',
            'type'    => 'radio_inline',
            'options' => array(
              array( 'name' => __( 'New', 'books' ), 'value' => 'new', ),
              array( 'name' => __( 'Used', 'books' ), 'value' => 'used', ),
            ),
          ),
          array(
            'name' => __( 'Featured Book', 'books' ),
            'desc' => __( 'Displays on homepage', 'books' ),
            'id'   => $prefix . 'featured',
            'type' => 'checkbox',
          ),
          array(
            'name' => __( 'Cover', 'books' ),
            'desc' => __( 'Upload an image or enter a URL.', 'books' ),
            'id'   => $prefix . 'cover_image',
            'type' => 'file',
          ),
          array(
            'name' => __( 'Related Page', 'books' ),
            'desc' => __( 'If none, leave blank', 'books' ),
            'id'   => $prefix . 'related_book',
            'type' => 'page_select',
          ),
        ),
      );
    
      // Add other metaboxes as needed
    
      return $meta_boxes;
    }

}//end the_bare_necessities_books_metabox

$the_bare_necessities_books_metabox = new the_bare_necessities_books_metabox();