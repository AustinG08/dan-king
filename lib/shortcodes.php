<?php
class the_bare_necessities_shortcodes{
    
    //add actions/hooks in construct
    function __construct(){
        add_shortcode('phone', array($this,'phone_number'));
        add_shortcode('share_buttons', array($this,'share_buttons'));
        add_shortcode('wufoo_form', array($this,'wufoo_form') );
        add_shortcode('google_map', array($this, 'google_map_shortcode') );
        add_shortcode('iframe', array($this, 'iframe_shortcode') );
    }//end __construct
    
    function phone_number(){
        global $theme_options;
        if( !empty($theme_options['phone_number']) ){
            return $theme_options['phone_number'];
        }
    }
    
    function share_buttons(){
        wp_enqueue_script('po.st-js', 'http://i.po.st/static/v3/post-widget.js#publisherKey=rc74kvlr9gej9t7nkuou', array(), false, true);
        return '<div class="share-buttons"><div class="pw-widget pw-size-medium"><a class="pw-button-facebook"></a><a class="pw-button-twitter"></a><a class="pw-button-email"></a><a class="pw-button-digg"></a><a class="pw-button-stumbleupon"></a><a class="pw-button-linkedin"></a><a class="pw-button-pinterest"></a><a class="pw-button-googleplus"></a></div></div>';
    }
    
    function wufoo_form($atts){
        extract( shortcode_atts( array(
            'username' => 'adviceinteractive',
            'id' => FALSE,
        ), $atts ) );
        if( $atts['id'] == false  OR !isset($atts['id']) ){
            return;
        }
        $output = "<div id=\"wufoo-" . $atts['id'] . "\">
        Fill out our <a href=\"http://" . $atts['username'] . ".wufoo.com/forms/" . $atts['id'] . "\">contact form</a>.
        </div>
        <script type=\"text/javascript\">var " . $atts['id'] . ";(function(d, t) {
        var s = d.createElement(t), options = {
        'userName':'" . $atts['username'] . "', 
        'formHash':'" . $atts['id'] . "', 
        'autoResize':true,
        'height':'717',
        'async':true,
        'header':'hide', 
        'ssl':true};
        s.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + 'wufoo.com/scripts/embed/form.js';
        s.onload = s.onreadystatechange = function() {
        var rs = this.readyState; if (rs) if (rs != 'complete') if (rs != 'loaded') return;
        try { " . $atts['id'] . " = new WufooForm();" . $atts['id'] . ".initialize(options);" . $atts['id'] . ".display(); } catch (e) {}};
        var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr);
        })(document, 'script');</script>";
       return trim($output);
    }
    
    function google_map_shortcode($atts){
        extract( shortcode_atts( array(
          'address' => '',
              'align' => '',
            ), $atts ) );
        //if no address is defined, bail
        if( empty($address) ){
          return;
        }
        $output = '';
        if( !empty($align) ){
            $output .= '<div class="flex-' . $align . '">';
        }
    $output .= '<div class="flex"><iframe src="https://www.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=' . urlencode($address . ' United States') . '&ie=UTF8&z=12&output=embed" width="600" height="450" frameborder="0" style="border:0"></iframe></div>';
        if( !empty($align) ){
            $output .= '</div>';
        }
        return $output;
    }//google_map_shortcode
      
    function iframe_shortcode($atts, $content = null){
        //if no content, bail
        if( empty($content) OR $content == null ){
          return;
        }
        extract( shortcode_atts( array(
          'align' => '',
        ), $atts ) );
        $output = '';
        if( !empty($align) ){
            $output .= '<div class="flex-' . $align . '">';
        }
        $output .= '<div class="flex">' . $content . '</div>';
        if( !empty($align) ){
            $output .= '</div>';
        }
        return $output;
    }//iframe_shortcode
    
}//end the_bare_necessities_shortcodes

$the_bare_necessities_shortcodes = new the_bare_necessities_shortcodes();