<?php
class the_bare_necessities_wordpress_misc{
    
    //add actions/hooks in construct
    function __construct(){
        add_action( 'init', array($this,'wordpress_misc') );
    }//end __construct
    
    function wordpress_misc(){
        //enable featured images
        if ( function_exists( 'add_theme_support' ) ){
            add_theme_support( 'post-thumbnails' );
        }
        //remove autop from shortcodes
        remove_filter( 'the_content', 'wpautop' );
        add_filter( 'the_content', 'wpautop' , 99);
        //allow shortcodes in widgets
        add_filter('widget_text', 'do_shortcode');
    }//end wordpress_misc
    
}//end the_bare_necessities_wordpress_misc


$the_bare_necessities_wordpress_misc = new the_bare_necessities_wordpress_misc();