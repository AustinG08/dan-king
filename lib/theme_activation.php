<?php
class the_bare_necessities_theme_activation{
    
    //add actions/hooks in construct
    function __construct(){
        add_action( 'after_switch_theme', array($this,'theme_activation') );
    }//end __construct
    
    function theme_activation(){
        
        //get global variables
        global $wpdb;
        global $wp_rewrite;
        
        //update permalink structure
        update_option('permalink_structure', '/%postname%/');
        
        //write to .htaccess
        $wp_rewrite->flush_rules( true );
        
        //select ID of hello world post from DB
        $hello_world = $wpdb->get_var(" SELECT ID FROM $wpdb->posts WHERE post_name = 'hello-world'; ");
        //if we have a result, delete the hello world post
        if( $hello_world != NULL ){
            wp_delete_post( intval($hello_world), true );
        }
        
        //select ID of sample page post from DB
        $sample_page = $wpdb->get_var(" SELECT ID FROM $wpdb->posts WHERE post_name = 'sample-page'; ");
        //if we have a result, delete the sample page
        if( $sample_page != NULL ){
            wp_delete_post( intval($sample_page), true );
        }
        
        //disable pingbacks and trackbacks
        update_option('default_ping_status', 'closed');
        
        //disable comments
        update_option('default_comment_status', 'closed');
        
        //disable comment emails
        update_option('comments_notify', 0);
        update_option('moderation_notify', 0);
        
        //get current tagline
        $tagline = get_option('blogdescription');
        //if tagline is default, remove it
        if( $tagline == 'Just another WordPress site' ){
            update_option('blogdescription', '');
        }
        
        //set front page to static page
        update_option('show_on_front', 'page');
        
        //set timezone to CST
        update_option('timezone_string', 'America/Chicago');
        
        //remove links around images inserted with wysiwyg
        update_option('image_default_link_type','none');
        
    }//end theme_activation
}//end the_bare_necessities_theme_activation

$the_bare_necessities_theme_activation = new the_bare_necessities_theme_activation();