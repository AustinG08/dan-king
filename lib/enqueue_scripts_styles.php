<?php
class the_bare_necessities_enqueue_scripts_styles{
    
    //add action/hooks in construct
    function __construct(){
        add_action( 'wp_enqueue_scripts', array($this, 'register_scripts') );
    }//end __construct
    
    function register_scripts()  {
        //if we are on admin (not front-end), bail
        if( is_admin() ){return;}
        
        //define $js_deps array for theme JS
        $js_deps = array('jquery');
        //define $css_deps array for theme CSS
        $css_deps = array();
        
        //enqueue jquery validate plugin in footer with jquery dependency
        wp_enqueue_script( 'jquery-validate', get_template_directory_uri() . '/vendors/jquery/validate/1.11.1/jquery.validate.min.js', array( 'jquery' ), '1.11.1', true );
        $js_deps[] = 'jquery-validate';
        
        //load HTML5shiv for IE and below
        if( ( preg_match('/(?i)IE 8/',$_SERVER['HTTP_USER_AGENT']) AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/4.0') !== false ) OR ( preg_match('/(?i)IE [5-7]/',$_SERVER['HTTP_USER_AGENT']) AND !stripos($_SERVER['HTTP_USER_AGENT'], 'Trident') ) ){
            wp_enqueue_script( 'html5shiv', get_template_directory_uri() . '/vendors/js/html5shiv/3.7.1/html5shiv.min.js', array(), '3.7.1', false );
        }
        
        //enqueue theme JS last with array of dependencies
        wp_enqueue_script( 'the-bare-necessities-js', get_template_directory_uri() . '/assets/js/the-bare-necessities.js', $js_deps, false, true );
        
        //pass WordPress variables to theme JS
        $js_variables = array(
            'ajax_url' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce("the-bare-necessities"),
            'template_directory' => get_template_directory_uri(),
        );
        wp_localize_script( 'the-bare-necessities-js', 'wordpress_variables', $js_variables );
        
        //enqueue theme stylesheet last for dependencies
        wp_enqueue_style( 'the-bare-necessities-style', get_stylesheet_directory_uri() . '/assets/scss/the-bare-necessities.scss', $css_deps );
    }//end register_scripts    
    
}//end the_bare_necessities_enqueue_scripts_styles

$the_bare_necessities_enqueue_scripts_styles = new the_bare_necessities_enqueue_scripts_styles();
