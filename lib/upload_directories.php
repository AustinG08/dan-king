<?php
class the_bare_necessities_uploads_directories{
  
  //add action/hooks in construct
  function __construct(){
    //set upload path to root
    update_option('upload_path', ABSPATH );
    //set upload url to root
    update_option('upload_url_path', get_bloginfo('url') );
    //disable month/year directories
    update_option('uploads_use_yearmonth_folders', '0' );
    //organize uploads by media type
    add_filter('wp_handle_upload_prefilter', array($this, 'upload_prefilter'));
    add_filter('wp_handle_upload', array($this, 'upload'));
  }//end __construct
  
  function upload_prefilter( $file ){
    add_filter('upload_dir', array($this, 'custom_upload_dir'));
    return $file;
  }//end upload_prefilter
  function upload( $fileinfo ){
    remove_filter('upload_dir', 'custom_upload_dir');
    return $fileinfo;
  }//end upload
  function custom_upload_dir($path){
    //Bail if there is an error
    if( !empty( $path['error'] ) ){
      return $path;
    }
    $extension = substr( strrchr( $_POST['name'], '.' ), 1 );
    switch( $extension ):
      case 'jpg':
      case 'jpeg':
      case 'png':
      case 'gif':
	$customdir = '/images';
	break;
      case 'mp4':
      case 'm4v':
      case 'mov':
      case 'wmv':
      case 'avi':
      case 'mpg':
      case 'ogv':
      case '3gp':
      case '3g2':
	$customdir = '/videos';
	break;
      case 'doc':
      case 'docx':
      case 'txt':
	$customdir = '/documents';
	break;
      case 'pdf':
	$customdir = '/pdf';
	break;
      case 'mp3':
      case 'm4a':
      case 'ogg':
      case 'wav':
	$customdir = '/audio';
	break;
      default:
	$customdir = '/media';
	break;
    endswitch;
    $path['path']  = ABSPATH;
    $path['url']   = get_bloginfo('url');
    $path['subdir']  = $customdir;
    $path['path']   .= $customdir;
    $path['url']  .= $customdir;
    return $path;
  }//end custom_upload_dir
}//end the_bare_necessities_uploads_directories

$the_bare_necessities_uploads_directories = new the_bare_necessities_uploads_directories();
?>