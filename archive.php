<?php get_header(); ?>
<section id="content" role="main" class="blog">
  <h1>
  <?php
  global $post;
  if( !empty($banner_text) ){
    echo $banner_text;
  }
  elseif ( is_category() ){
    echo '"' . single_cat_title( $prefix = '', false ) . '" Posts';
  }
  elseif( is_tag() ){
    echo 'Posts Tagged "' . single_tag_title( $prefix = '', false ) . '"';
  }
  elseif ( is_day() ){
    echo 'Posts on  ' . get_the_time('F jS, Y');
  }
  elseif ( is_month() ){
    echo 'Posts in  ' . get_the_time('F, Y'); 
  }
  elseif ( is_year() ){
    echo 'Posts in  ' . get_the_time('Y'); 
  }
  elseif ( is_author() ){
    echo 'Posts by ' . get_author_name(get_query_var('author'));
  }
  elseif( is_search() ){
    echo 'Search results for "' . $_GET['s'] . '"';
  }
  global $paged;
  if ( !empty($paged) ){
    echo ',  Page ' . $paged;
  }
  ?>
  </h1>
    <?php
    $count = 1;
    while( have_posts() ):
   the_post();
    ?>
    <div class="post <?php echo ( $count %2 == 0 ) ? 'even' : 'odd'; ?>">
        <div class="title">
          <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        </div><!--//title-->
        <div class="post-meta clear">
        Posted in <?php the_category(', '); ?> by <a href="<?php echo get_author_posts_url($post->post_author); ?>"><?php the_author(); ?></a>
      </div><!--//post-meta-->
        <div class="excerpt">
          <?php the_excerpt(); ?>
        </div><!--//excerpt-->
    </div><!--//post-->
    <?php
    $count++;
    endwhile;
    $the_bare_necessities_theme->pagination();
    ?>
</section><!--//content-->
<?php get_footer(); ?>