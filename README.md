#The Bare Necessities
#A Custom WordPress Starter Theme for Advice Interactive Group

###README incomplete

##Git Clone
To clone this project use --recursive to properly download submodules

```git clone --recursive git@bitbucket.org:aigdev/wp-theme-the-bare-necessities.git my_theme_name```

##Documentation
For documentation please visit the [Wiki](https://bitbucket.org/aigdev/the-bare-necessities/wiki/Home)

##Vendor Items
* [jQuery Validation](http://jqueryvalidation.org/)
* [Theme Options](http://reduxframework.com/)
* [Metaboxes](https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress/)

###TODO:
* Add additional validation methods to theme options
  * phone
  * regex replace
* Add class exists conditionals around modules
* Add [show_on](https://github.com/WebDevStudios/Custom-Metaboxes-and-Fields-for-WordPress/wiki/Adding-your-own-show_on-filters) filters for metaboxes
