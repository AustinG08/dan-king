<?php get_header(); ?>
<div id="content" role="main">
  <div class="inner">
  <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
    <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
      <header>
      <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
      <div class="post-meta clear">
        Posted in <?php the_category(', '); ?> by <a href="<?php echo get_author_posts_url($post->post_author); ?>"><?php the_author(); ?></a>
      </div><!--//post-meta-->
      </header>
      <?php the_excerpt('Read the rest of this entry &raquo;'); ?>
    </article>
    <?php endwhile; ?>
  <?php else : ?>
    <h2>Nothing Found</h2>
    <p>Sorry, but it looks like there isn't any content yet</p>
  <?php endif; ?>
  </div><!--//inner-->
</div><!--//content-->
<?php get_footer(); ?>