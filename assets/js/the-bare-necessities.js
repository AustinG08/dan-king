/* DESKTOP MOBILE CLASSES */
function desktop_mobile_classes() {
  var window_size = jQuery('.breakpoint').css('content').replace(/"/g, "");
  if ( window_size === 'mobile' ) {
    if ( jQuery('body').hasClass('desktop') ) {
      jQuery('body').removeClass('desktop');
    }
    if ( jQuery('body').hasClass('portrait') ) {
      jQuery('body').removeClass('portrait');
    }
    if ( jQuery('body').hasClass('landscape') ) {
      jQuery('body').removeClass('landscape');
    }
    jQuery('body').addClass('mobile');
  }
  else if ( window_size === 'portrait' ) {
    if ( jQuery('body').hasClass('desktop') ) {
      jQuery('body').removeClass('desktop');
    }
    if ( jQuery('body').hasClass('mobile') ) {
      jQuery('body').removeClass('mobile');
    }
    if ( jQuery('body').hasClass('landscape') ) {
      jQuery('body').removeClass('landscape');
    }
    jQuery('body').addClass('portrait');
  }
  else if ( window_size === 'landscape' ) {
    if ( jQuery('body').hasClass('desktop') ) {
      jQuery('body').removeClass('desktop');
    }
    if ( jQuery('body').hasClass('mobile') ) {
      jQuery('body').removeClass('mobile');
    }
    if ( jQuery('body').hasClass('portrait') ) {
      jQuery('body').removeClass('portrait');
    }
    jQuery('body').addClass('landscape');
  }
  else if( window_size === 'desktop' ) {
    if ( jQuery('body').hasClass('landscape') ) {
      jQuery('body').removeClass('landscape');
    }
    if ( jQuery('body').hasClass('mobile') ) {
      jQuery('body').removeClass('mobile');
    }
    if ( jQuery('body').hasClass('portrait') ) {
      jQuery('body').removeClass('portrait');
    }
    jQuery('body').addClass('desktop');
  }
}//desktop_mobile_classes

desktop_mobile_classes();
jQuery(window).resize(function(){desktop_mobile_classes();});

jQuery(document).ready(function($) {

  /* ADD ACTIVE CLASS TO CURRENT PAGINATION LI */
  $('ul.pagination span.current').parent('li').addClass('current');
  
  /* PLACEHOLDER SUPPORT */
  //https://github.com/mathiasbynens/jquery-placeholder/
  //check if placeholder support exists
  var placeholderSupport = "placeholder" in document.createElement("input");
  //if placeholder support does not exist
  if ( placeholderSupport === false ) {
    //load placeholder support script and initialize
    $.getScript( wordpress_variables.template_directory + "/vendors/jquery/placeholder/2.0.7/jquery.placeholder.min.js" ).done(function(){$('input, textarea').placeholder();});
  }
  
  /* FORM VALIDATION */
  //add phone # validation method
  jQuery.validator.addMethod("phoneUS", function(phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, "");
    return this.optional(element) || phone_number.length > 9 &&
    phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
  }, "Please enter a valid phone number");
  
  /* SMOOTH ANCHOR SCROLLING */
  $(function() {
    $('a[href*=#]:not([href=#])').click(function(e) {
      $('body').removeClass('nav-active');
      e.preventDefault();
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	var target = $(this.hash);
	target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	if (target.length) {
	  var offSet = target.offset().top - 25;
	  $('html,body').animate({
	    scrollTop: offSet
	  }, 1000);
	}
      }
    });
  });
  
});//end doc ready
