<?php
/*  WRAP THEME FUNCTIONS IN A CLASS FOR NAMESPACING */
class the_bare_necessities_theme{
    
    //add actions/hooks in construct
    function __construct(){
        //register main menus
        register_nav_menus( array('main_menu' => 'Main Nav Menu') );
        add_action('wp_footer', array($this,'add_theme_options_code_to_footer'), 999999 );
        add_action('wp_head', array($this,'add_theme_options_code_to_head'), 999999 );
        add_action('wp_head', array($this,'add_viewport_meta_to_head'), 2 );
    }//end __construct
    
    
    //add JS from theme options to footer
    function add_theme_options_code_to_footer(){
        global $theme_options;
        echo $theme_options['footer_code'] . "\n";
    }//end add_theme_options_code_to_footer
    
    //add JS from theme options to head
    function add_theme_options_code_to_head(){
        global $theme_options;
        echo $theme_options['head_code'] . "\n";
    }//end add_theme_options_code_to_head
    
    //Pagination
    public function pagination($query=false) {
        global $wp_query;
        if( !$query ){
          $query = $wp_query;
        }
        $big = 999999999;
        $links = paginate_links(array(
          'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
          'format' => '?paged=%#%',
          'prev_next' => true,
          'prev_text' => '&laquo;',
          'next_text' => '&raquo;',
          'current' => max( 1, get_query_var('paged') ),
          'total' => $query->max_num_pages,
          'type' => 'list'
        ));//end $links
        $pagination = str_replace('page-numbers','pagination',$links);
        echo $pagination;
    }//end pagination
    
    function add_viewport_meta_to_head(){
        echo '<meta name="HandheldFriendly" content="True" />' . "\n";
        echo '<meta name="MobileOptimized" content="320" />' . "\n";
        echo '<meta name="viewport" content="width=device-width, initial-scale=1.0" />' . "\n";
    }//end add_viewport_meta_to_head
    
}//end the_bare_necessities_theme

/* INITIALIZE THE THEME CLASS */
$the_bare_necessities_theme = new the_bare_necessities_theme();

/* INCLUDE MODULES */
get_template_part('lib/theme_activation');
get_template_part('lib/add_post_id_to_admin');
get_template_part('lib/wordpress_misc');
get_template_part('lib/enqueue_scripts_styles');
get_template_part('lib/upload_directories');
get_template_part('lib/theme_options');
get_template_part('lib/metabox_init');
get_template_part('lib/shortcodes');
//get_template_part('lib/books_cpt');
//get_template_part('lib/books_metabox');