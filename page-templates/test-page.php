<?php
/* Template Name: Test Page */
get_header();
?>
  <div id="content" role="main" class="page">
    <?php
    global $theme_options;
    if( !empty($theme_options) ){
      echo '<h1>Theme Option Values</h1>';
      foreach( $theme_options as $key=>$value ){
        if( empty($value) ){
          continue;
        }
        echo '<h3>' . $key . '</h3>';
        echo '<p><code>' . htmlspecialchars(print_r($value, true)) . '</code></p>';
      }
      echo '<br /><br />';
    }
    if (have_posts()):
      while(have_posts()):
        the_post();
        the_content();
      endwhile;
    endif;
    ?>
  </div><!--//content-->
<?php
//get_sidebar();
get_footer();
?>