<?php
/* when a custom menu walker just won't cut it you get nested arrays of doom */
global $post;
$current_page = $post->ID;
$registered_menus = get_nav_menu_locations();
if (isset($registered_menus['main_menu'])):
    $main_menu = wp_get_nav_menu_object( $registered_menus['main_menu'] );
    $parents = wp_get_nav_menu_items($main_menu->term_id, array('meta_key' => '_menu_item_menu_item_parent', 'meta_value' => 0));
    $nav_items = array();
    $parent_count = 0;
    foreach( $parents as $parent ):
        $nav_items[$parent_count] = array(
            'active' => ( $current_page == intval($parent->object_id) ? true : false),
            'url' => $parent->url,
            'title' => $parent->title,
            'id' => $parent->object_id,
            'children' => array()
        );
        $children = wp_get_nav_menu_items($main_menu->term_id, array('meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $parent->ID));
        if( !empty($children) ):
            $child_count = 0;
            foreach($children as $child):
                $nav_items[$parent_count]['children'][$child_count] = array(
                    'active' => ( $current_page == intval($child->object_id) ? true : false),
                    'url' => $child->url,
                    'title' => $child->title,
                    'id' => $child->object_id,
                    'children' => array(),
                );
                if( $current_page == intval($child->object_id) ){
                    $nav_items[$parent_count]['active'] = true;
                }
                $grand_children = wp_get_nav_menu_items($main_menu->term_id, array('meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $child->ID));
                if( !empty($children) ):
                    $grand_child_count = 0;
                    foreach($grand_children as $grand_child):
                        $nav_items[$parent_count]['children'][$child_count]['children'][$grand_child_count] = array(
                            'active' => ( $current_page == intval($grand_child->object_id) ? true : false),
                            'url' => $grand_child->url,
                            'title' => $grand_child->title,
                            'id' => $grand_child->object_id,
                        );
                        if( $current_page == intval($grand_child->object_id) ){
                            $nav_items[$parent_count]['active'] = true;
                            $nav_items[$parent_count]['children'][$child_count]['active'] = true;
                        }
                        $grand_child_count++;
                    endforeach;//end grand children
                endif;
                $child_count++;
            endforeach;//end children
        endif;
        $parent_count++;
    endforeach;//end parents
    $ul_class = 'level-0';
    echo '<ul class="' . $ul_class . '">';
        foreach($nav_items as $i=>$parent):
            $a_class = $li_class = 'level-0';
            if( $parent['active'] ){
                $a_class .= ' active';
            }
            if( !empty($parent['children']) ){
                $li_class .= ' has-children';
                $icon_class = 'fa-chevron-down';
            } else{
                $icon_class = false;
            }
            echo '<li class="' . $li_class . '">';
                echo '<a href="' . $parent['url'] . '" class="' . $a_class . '">';
                    echo $parent['title'];
                    if( $icon_class != false ){
                        echo '<span class="icon"><i class="fa ' . $icon_class . '"></i></span>';
                    }
                echo '</a>';
            if( !empty($parent['children']) ):
                $ul_class = 'sub-menu level-1';
                echo '<ul class="' . $ul_class . '">';
                foreach( $parent['children'] as $j=>$child ):
                    $a_class = $li_class = 'level-1';
                    if( $child['active'] ){
                        $a_class .= ' active';
                        $li_class .= ' active';
                    }
                    if( !empty($child['children']) ){
                        $li_class .= ' has-children';
                        $icon_class = 'fa-chevron-down';
                    } else{
                        $icon_class = false;
                    }
                    echo '<li class="' . $li_class . '">';
                        echo '<a href="' . $child['url'] . '" class="' . $a_class . '">';
                            echo $child['title'];
                            if( $icon_class != false ){
                                echo '<span class="icon"><i class="fa ' . $icon_class . '"></i></span>';
                            }
                        echo '</a>';
                    if( !empty($child['children']) ):
                        $ul_class = 'sub-menu level-2';
                        echo '<ul class="' . $ul_class . '">';
                        foreach( $child['children'] as $k=>$grand_child ):
                            $a_class = $li_class = 'level-2';
                            if( $grand_child['active'] ){
                                $a_class .= ' active';
                            }
                            echo '<li class="' . $li_class . '">';
                                echo '<a href="' . $grand_child['url'] . '" class="' . $a_class . '">';
                                    echo $grand_child['title'];
                                echo '</a>';
                            echo '</li>';
                        endforeach;//end grand children
                        echo '</ul>';
                    endif;
                    echo '</li>';
                endforeach;//end children
                echo '</ul>';
            endif;
            echo '</li>';
        endforeach;//end parent
    echo '</ul>';
endif;
?>